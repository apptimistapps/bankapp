//
//  Bank.swift
//  BasicBankApp
//
//  Created by Dee Odus on 27/12/2021.
//

import Foundation

struct Bank{
	
	let name: String = "Basic Account"
	let balance: Float = 0
	let user: User
}
