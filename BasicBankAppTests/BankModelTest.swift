//
//  BankModelTest.swift
//  BasicBankAppTests
//
//  Created by Dee Odus on 27/12/2021.
//

import Foundation
import XCTest

@testable import BasicBankApp

class BankModelTests: XCTestCase{
	
	
	func testCreateBankModel_InitialisedCorrectly(){
		
		let user = User()
		let bank = Bank(user: user)
		
		XCTAssertEqual(bank.balance, 0)
		XCTAssertEqual(bank.name, "Basic Account")
		
	}
}

